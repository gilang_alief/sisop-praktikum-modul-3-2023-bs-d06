#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include<wait.h>
int readfile(char* argv, int frequency[]){

    FILE *fp;
    int c;

    fp = fopen(argv, "r");
    if (fp == NULL) {
        printf("Error opening the file\n");
        return 1;
    }

    while ((c = fgetc(fp)) != EOF) {
        if (c >= 'a' && c <= 'z') {
            frequency[c - 'a']++;
        } else if (c >= 'A' && c <= 'Z') {
            frequency[c - 'A']++;
        }
    }

    fclose(fp);

    for (int i = 0; i < 26; i++) {
        printf("%c: %d\n", 'a' + i, frequency[i]);
    }
    return frequency;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }
  int frequency[26] = {0}, sz = sizeof(frequency)/sizeof(frequency[0]);
  int pfd[2];
    if (pipe(pfd) == -1) {
        printf("Pipe failed\n");
        exit(1);
    }

    // create child process
    pid_t pd = fork();
    if(pd>0){
        readfile(argv[1], frequency);
    }


  
    return 0;
}