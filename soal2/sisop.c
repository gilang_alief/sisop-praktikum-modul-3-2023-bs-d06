#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
int armat[4][5];
unsigned long long factorial(unsigned long long n) {
    if (n == 0) {
        return 1;
    } else {
        return n * factorial(n-1);
    }
}
int main() {
    int smid;
    key_t key = 4869;
    int *sm;
    int i, j;
   clock_t start, end;
    
 srand(time(NULL));
 start = clock();
    smid = shmget(key, sizeof(int) * 4 * 5, 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }

    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            armat[i][j] = *(sm + i * 5 + j);
        }
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", armat[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    shmdt(sm);
    printf("\narray: [");
for (i = 0; i < 4; i++) {
    printf("[");
        for (j = 0; j < 5; j++) {
            if (j== 4){
            printf("%d", armat[i][j]);
            }else printf("%d, ", armat[i][j]);
        }
        if(i==3){
            printf("]");
        }else printf("], ");
    }
    printf("] \n\nmaka:\n \n");

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%llu ", factorial(armat[i][j]));
        }
        printf(" ");
    }
 end = clock();
    printf("\ntime: %f sec\n", ((double) (end - start)) / CLOCKS_PER_SEC);
    return 0;
}

