#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>
int armat[4][5];
unsigned long long hasilf[20];
int c=0;
void *dofac(void *arg) {
    int *args = (int *) arg;
    int i = args[0];
    int j = args[1];
    int n = armat[i][j];
    unsigned long long res = 1;
    unsigned long long k;
    for (k = 2; k <= n; k++) {
        res *= k;
    }
    hasilf[c] = res;
    c++;
    pthread_exit(NULL);
}

int main() {
    int smid;
    key_t key = 4869;
    int *sm;
    int i, j;
    clock_t start, end;
    
 srand(time(NULL));
 start = clock();
    smid = shmget(key, sizeof(int) * 4 * 5, 0666);
    if (smid < 0) {
        perror("shmget");
        exit(1);
    }

    sm = shmat(smid, NULL, 0);
    if (sm == (int *) -1) {
        perror("shmat");
        exit(1);
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            armat[i][j] = *(sm + i * 5 + j);
        }
    }
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", armat[i][j]);
        }
        printf("\n");
    }
    printf("\n");

   
    shmdt(sm);
// Create threads to calculate dofac of each number in the armat
    pthread_t threads[20];
    int thread_args[20][2];
    int count = 0;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            thread_args[count][0] = i;
            thread_args[count][1] = j;
            pthread_create(&threads[count], NULL, dofac, (void *) &thread_args[count]);
            count++;
        }
    }

    // Wait for all threads to finish
    for (i = 0; i < 20; i++) {
        pthread_join(threads[i], NULL);
    }
    printf("\narray: [");
for (i = 0; i < 4; i++) {
    printf("[");
        for (j = 0; j < 5; j++) {
            if (j== 4){
            printf("%d", armat[i][j]);
            }else printf("%d, ", armat[i][j]);
        }
        if(i==3){
            printf("]");
        }else printf("], ");
    }

    printf("] \n\nmaka:\n \n");
    for (i = 0; i < 20; i++) {
            printf("%llu ", hasilf[i]);
    }
    end = clock();
    printf("\ntime: %f sec\n", ((double) (end - start)) / CLOCKS_PER_SEC);
    return 0;
}
